package TA_032_EA2_E3;

public class AnalizadorLexico {
    private String texto;

    public AnalizadorLexico(String texto) {
        this.texto = texto;
    }

    //i->matricula
    //j->primer nombre
    //w->iniciales apellido
    //r->iniciales apellido inverso

    public boolean startsWith(char tipo)
    {
        switch(tipo)
        {
            case 'i':
                return texto.startsWith("1907852");
            case 'j':
                return texto.startsWith("sergio");
            case 'w':
                return texto.startsWith("er");
            case 'r':
                return texto.startsWith("re");
        }
        return false;
    }

    public boolean endsWith(char tipo)
    {
        switch(tipo)
        {
            case 'i':
                return texto.endsWith("1907852");
            case 'j':
                return texto.endsWith("sergio");
            case 'w':
                return texto.endsWith("er");
            case 'r':
                return texto.endsWith("re");
        }
        return false;
    }

    public boolean stringEquals(char tipo)
    {
        switch(tipo)
        {
            case 'i':
                return texto.equals("1907852");
            case 'j':
                return texto.equals("sergio");
            case 'w':
                return texto.equals("er");
            case 'r':
                return texto.equals("re");
        }
        return false;
    }

    //v de void, por si se necesita hacer un substring de solo un lado
    public int getTipoLenght(char tipo)
    {
        switch(tipo)
        {
            case 'v':
                return 0;
            case 'i':
                return 7;
            case 'j':
                return 6;
            case 'w':
            case 'r':
                return 2;
        }
        return 0;
    }

    public void substring(char tipoInicio, char tipoFinal)
    {
        int slenght = getTipoLenght(tipoInicio);
        int elenght = getTipoLenght(tipoFinal);
        texto = texto.substring(slenght, texto.length() - elenght);
    }
}