package TA_032_EA2_E3;

import java.util.Scanner;

public class Main {
    static Scanner escaner = new Scanner(System.in);

    public static void main(String[] args) {
	    String entrada; // cadena de entrada

        do {
            System.out.println("Introduzca una cadena de caracteres: ");
            entrada = escaner.nextLine();
            if (esValidaParaGramatica(entrada))
                System.out.println("La cadena es válida para la gramática.");
            else
                System.out.println("La cadena NO es válida para la gramática.");
        } while (analizarOtra());
    }

    public static boolean analizarOtra() {
        System.out.println("¿Desea analizar otra cadena? (s - Sí, n - No)");
        String respuesta = escaner.nextLine();
        if (respuesta.equalsIgnoreCase("s"))
            return true;
        else if (respuesta.equalsIgnoreCase("n"))
            return false;

        System.out.println("Entrada incorrecta");
        return analizarOtra();
    }

    public static boolean esValidaParaGramatica(String entrada) {
        String w = "er";    // Apellidos: Elizondo Rodríguez
        String wi = "re";   // w inversa
        String i = "1907852";   // Matrícula: 1907852
        String j = "sergio";    // Primer nombre: Sergio

        AnalizadorSintactico analizadorSintactico = new AnalizadorSintactico(entrada);
        return analizadorSintactico.cadenaValida();
    }
}
