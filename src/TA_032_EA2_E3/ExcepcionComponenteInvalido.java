package TA_032_EA2_E3;

public class ExcepcionComponenteInvalido extends Exception {
    public ExcepcionComponenteInvalido(String mensaje) {
        super(mensaje);
    }
}
