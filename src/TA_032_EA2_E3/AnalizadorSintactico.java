package TA_032_EA2_E3;

public class AnalizadorSintactico {
    private AnalizadorLexico analizadorLexico;

    public AnalizadorSintactico(String texto) {
        analizadorLexico = new AnalizadorLexico(texto);
    }

    public boolean cadenaValida() {
        try {
            return analizarEstadoS();
        }
        catch (Exception excepcion) {
            return false;
        }
    }

    private boolean analizarEstadoS() throws Exception {

        if(!analizadorLexico.startsWith('i'))
            throw new Exception("No empieza con i");
        if(!analizadorLexico.endsWith('j'))
            throw new Exception("No termina con j");

        analizadorLexico.substring('i', 'j');

        if(!analizadorLexico.endsWith('j'))
            throw new Exception("No termina con doble j");

        analizadorLexico.substring('v', 'j');

        return analizarEstadoA();
    }

    private boolean analizarEstadoA() throws Exception {

        if(!analizadorLexico.startsWith('w'))
            throw new Exception("No empieza con w");
        if(!analizadorLexico.endsWith('r'))
            throw new Exception("No termina con r (w')");

        analizadorLexico.substring('w', 'r');

        if(!analizadorLexico.endsWith('r'))
            throw new Exception("No termina con doble r (w')");

        analizadorLexico.substring('v', 'r');

        if(analizadorLexico.stringEquals('i'))
            return true;
        else
            return analizarEstadoA();
    }
}
